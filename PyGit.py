#!/usr/bin/env python
import sys
import os

#Execute git add, git commit, git push commands
def add_commit_push():

    message = input("Enter a git commit message: ")
    stream = os.popen("git --no-pager branch")
    output = stream.read()
    print(output)

    branch = input("Which branch do you want to push to? ")
    stream = os.popen(f"git add -A; \
        git commit -m '{message}'; \
        git push origin {branch}; git push github {branch};")
    output = stream.read()
    print(output)

if __name__ == "__main__":
    stream = os.popen("git status")
    output = stream.read()
    if output.strip().endswith("nothing to commit, working tree clean"):
        print(output)
    else:
        add_commit_push()