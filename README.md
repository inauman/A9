# A9 (Automation) Project
A set of simple python utilities to automate and simplify the repeating and boring tasks.

## 1. PyGit: Automate "git add, commit, push" commands.
Pushing code changes to repo, in small increments, require executing git add, commit, and push commands quite frequently. It's quite annoying and distracting!

PyGit is a simple python utility which combine three git commands into one program and make it quick and easy to check-in code in the repo.
```
$./PyGit "Code comment goes here."
```